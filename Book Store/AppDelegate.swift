//
//  AppDelegate.swift
//  Book Store
//
//  Created by Mukhammadyunus on 8/11/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        
        window?.rootViewController = cWalkthroughVC()
        window?.makeKeyAndVisible()
        
        return true
    }



}

