//
//  WalkthroughCVCView.swift
//  Book Store
//
//  Created by Rakhmatillo Topiboldiev on 8/14/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

class vWalkthroughCVCView {
    
    var contentView: UIView!
    let label1 = UILabel()
    let label2 = UILabel()
    let imgView = UIImageView()
    
    let stackViewForAll = UIStackView()
    let stackViewForLabels = UIStackView()
    
    init(contentView: UIView) {
        self.contentView = contentView
        initCVCUI()
    }
    
    
    func initCVCUI(){
        
        contentView.addSubview(stackViewForAll)
        stackViewForAll.translatesAutoresizingMaskIntoConstraints = false
        stackViewForAll.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20).isActive = true
        stackViewForAll.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20).isActive = true
        stackViewForAll.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
        stackViewForAll.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive = true
        stackViewForAll.axis = .vertical
        stackViewForAll.distribution = .equalSpacing
        stackViewForAll.spacing = 30
        stackViewForAll.alignment = .fill
        
        setUpLabels()
        
    }
    
    
    func setUpLabels(){
        stackViewForAll.addArrangedSubview(stackViewForLabels)
        stackViewForAll.addArrangedSubview(imgView)
        stackViewForLabels.translatesAutoresizingMaskIntoConstraints = false
        stackViewForLabels.addArrangedSubview(label1)
        stackViewForLabels.addArrangedSubview(label2)
        stackViewForLabels.axis = .vertical
        stackViewForLabels.distribution = .fill
        stackViewForLabels.alignment = .fill
        stackViewForLabels.spacing = 15
        
        label1.translatesAutoresizingMaskIntoConstraints = false
        label2.translatesAutoresizingMaskIntoConstraints = false
        
        label1.font = UIFont.systemFont(ofSize: 28, weight: .semibold)
        label1.textColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
        label1.numberOfLines = 0
        label1.textAlignment = .center
        label1.text = ""
        
        label2.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label2.textColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
        label2.numberOfLines = 0
        label2.textAlignment = .center
        label2.text = ""
        
        
        imgView.translatesAutoresizingMaskIntoConstraints = false
        imgView.contentMode = .scaleToFill
        imgView.heightAnchor.constraint(equalToConstant: contentView.frame.width - 100).isActive = true

        
    }
    
}
