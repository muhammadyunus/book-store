//
//  WalkthroughView.swift
//  Book Store
//
//  Created by Rakhmatillo Topiboldiev on 8/13/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

class vWalkthroughView {
    var view: UIView!
    var collectionView : UICollectionView?
    let girlImgView = UIImageView()
    let leftLeafImgView = UIImageView()
    let rightLeafImgView = UIImageView()
    let lastViewTitleLbl = UILabel()
    let stackViewForBtn = UIStackView()
    let signInBtn = UIButton(type: .roundedRect)
    let signUpBtn = UIButton(type: .roundedRect)
    let pageControl = UIPageControl()

    init(view: UIView) {
        self.view = view
        initUI()
    }
   
    func initUI(){
        self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        setUpCollectionView()
        setLastView()
    }
    
    
    //MARK: - SET COLLECTION VIEW
    func setUpCollectionView(){
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: view.frame, collectionViewLayout: layout)
        self.view.addSubview(collectionView!)
        self.view.addSubview(pageControl)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = collectionView!.frame.size
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        
        collectionView!.isPagingEnabled = true
        collectionView!.alwaysBounceVertical = false
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.backgroundColor = .clear
        collectionView?.register(cWalkthroughCVC.self, forCellWithReuseIdentifier: "CVC")
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView?.heightAnchor.constraint(equalToConstant: self.view.frame.height * 0.6).isActive = true
        collectionView?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        collectionView?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        collectionView?.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        collectionView?.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        
        pageControl.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -70).isActive = true
        pageControl.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor, constant: 20).isActive = true
        pageControl.trailingAnchor.constraint(greaterThanOrEqualTo: view.trailingAnchor, constant: -20).isActive = true
        pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 0.2504280822)
        pageControl.numberOfPages = 4
        pageControl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        
        
        
        
    }
    
    //MARK: - LAST VIEW
       func setLastView(){
           view.addSubview(stackViewForBtn)
           view.addSubview(girlImgView)
           view.addSubview(leftLeafImgView)
           view.addSubview(rightLeafImgView)
           view.addSubview(lastViewTitleLbl)
           
           girlImgView.image = #imageLiteral(resourceName: "girl")
           girlImgView.contentMode = .scaleAspectFit
           girlImgView.isHidden = true
           girlImgView.translatesAutoresizingMaskIntoConstraints = false
           girlImgView.frame = CGRect(x: 0, y: 0, width: view.frame.width * 1.2, height: view.frame.height / 2.5)
           girlImgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -(view.frame.width / 2.3)).isActive = true
           girlImgView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
           girlImgView.heightAnchor.constraint(equalToConstant: view.frame.height / 2.5).isActive = true
           girlImgView.widthAnchor.constraint(equalToConstant: view.frame.width * 1.2).isActive = true
           
          
           leftLeafImgView.image = #imageLiteral(resourceName: "leaf1")
           leftLeafImgView.contentMode = .scaleToFill
           leftLeafImgView.isHidden = true
           leftLeafImgView.translatesAutoresizingMaskIntoConstraints = false
           
           leftLeafImgView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: -25).isActive = true
           leftLeafImgView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 10).isActive = true
           leftLeafImgView.heightAnchor.constraint(equalToConstant: view.frame.height / 3).isActive = true
           leftLeafImgView.widthAnchor.constraint(equalToConstant: 100).isActive = true
           
           
           rightLeafImgView.image = #imageLiteral(resourceName: "leaf")
           rightLeafImgView.contentMode = .scaleToFill
           rightLeafImgView.isHidden = true
           rightLeafImgView.translatesAutoresizingMaskIntoConstraints = false
           rightLeafImgView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height / 8).isActive = true
           rightLeafImgView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 90).isActive = true
           rightLeafImgView.heightAnchor.constraint(equalToConstant: view.frame.height / 3.5).isActive = true
           rightLeafImgView.widthAnchor.constraint(equalToConstant: view.frame.width / 2.5).isActive = true
           
           stackViewForBtn.addArrangedSubview(signInBtn)
           stackViewForBtn.addArrangedSubview(signUpBtn)
           stackViewForBtn.isHidden = true
           stackViewForBtn.translatesAutoresizingMaskIntoConstraints = false
           stackViewForBtn.axis = .vertical
           stackViewForBtn.distribution = .fillEqually
           stackViewForBtn.spacing = 20
           stackViewForBtn.widthAnchor.constraint(equalToConstant: view.frame.width - 50).isActive = true
           stackViewForBtn.heightAnchor.constraint(equalToConstant: 120).isActive = true
           stackViewForBtn.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20).isActive = true
           stackViewForBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
           
           
          
           signInBtn.setTitle("Sign In", for: .normal)
           signInBtn.backgroundColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
           signInBtn.layer.cornerRadius = 20
           signInBtn.tintColor = .white
           
           
           signUpBtn.setTitle("Sign Up", for: .normal)
           signUpBtn.backgroundColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
           signUpBtn.layer.cornerRadius = 20
           signUpBtn.tintColor = .white

           
           
           lastViewTitleLbl.text = "Books For Every Taste."
           lastViewTitleLbl.numberOfLines = 2
           lastViewTitleLbl.font = UIFont.systemFont(ofSize: 35, weight: .bold)
           lastViewTitleLbl.textColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
           lastViewTitleLbl.textAlignment = .center
           lastViewTitleLbl.isHidden = true
           lastViewTitleLbl.translatesAutoresizingMaskIntoConstraints = false
           lastViewTitleLbl.leadingAnchor.constraint(equalTo: leftLeafImgView.trailingAnchor, constant: 10).isActive = true
           lastViewTitleLbl.trailingAnchor.constraint(equalTo: rightLeafImgView.leadingAnchor, constant: -10).isActive = true
           lastViewTitleLbl.centerYAnchor.constraint(equalTo: leftLeafImgView.centerYAnchor).isActive = true
           
       }
    
    func changePageController(page: CGFloat) {
        if page <= 2{
            pageControl.currentPage = Int(page)
            self.leftLeafImgView.isHidden = true
            self.rightLeafImgView.isHidden = true
            self.girlImgView.isHidden = true
            self.stackViewForBtn.isHidden = true
            self.lastViewTitleLbl.isHidden = true

        }
        if page == 3{
            pageControl.currentPage = Int(page)
            
            girlImgView.transform = CGAffineTransform(translationX: -(view.frame.width), y: 0)
            leftLeafImgView.transform = CGAffineTransform(translationX: -50, y: 0)
            rightLeafImgView.transform = CGAffineTransform(translationX: 50, y: 0)
            lastViewTitleLbl.transform = CGAffineTransform(translationX: view.frame.width, y: 0)
            
            self.leftLeafImgView.isHidden = false
            self.rightLeafImgView.isHidden = false
            self.girlImgView.isHidden = false
            self.stackViewForBtn.isHidden = false
            self.lastViewTitleLbl.isHidden = false

            
            UIView.animate(withDuration: 0.3) {
                
                self.girlImgView.transform = .identity
                self.leftLeafImgView.transform = .identity
                self.rightLeafImgView.transform = .identity
                self.lastViewTitleLbl.transform = .identity
            }
            
        }
    }
    
    
   
   
}
