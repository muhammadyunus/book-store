//
//  DataModel.swift
//  Book Store
//
//  Created by Rakhmatillo Topiboldiev on 8/13/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

struct mWalkthroughDM {
    var img: UIImage
    var title: String
    var subTitle: String
}
