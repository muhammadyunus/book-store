//
//  CVC.swift
//  Book Store
//
//  Created by Rakhmatillo Topiboldiev on 8/11/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

class cWalkthroughCVC: UICollectionViewCell {
    var label1 = UILabel()
    var label2 = UILabel()
    var imgView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        let obj = vWalkthroughCVCView(contentView: contentView)
        self.label1 = obj.label1
        self.label2 = obj.label2
        self.imgView = obj.imgView

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    func updateCell(data: mWalkthroughDM) {
        imgView.image = data.img
        label1.text = data.title
        label2.text = data.subTitle
        
        
    }
    
}
