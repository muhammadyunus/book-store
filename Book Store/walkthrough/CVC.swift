//
//  CVC.swift
//  Book Store
//
//  Created by Rakhmatillo Topiboldiev on 8/11/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit

class WalkthroughCVC: UICollectionViewCell {
    let label1 = UILabel()
    let label2 = UILabel()
    let imgView = UIImageView()
    
    let stackViewForAll = UIStackView()
    let stackViewForLabels = UIStackView()

    override func awakeFromNib() {
        initUI()
    }
    
    func initUI(){
        stackViewForAll.axis = .vertical
        stackViewForAll.distribution = .fillEqually
        stackViewForAll.spacing = 35
        self.contentView.addSubview(stackViewForAll)
        setUpLabels()
        
    }
    
    
    func setUpLabels(){
        
        label1.font = UIFont.systemFont(ofSize: 25, weight: .semibold)
        label1.textColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
        label1.numberOfLines = 2
        label1.text = "Discounted Secondhand Books"
        stackViewForLabels.addArrangedSubview(label1)
        
        label2.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label2.textColor = #colorLiteral(red: 0.3529411765, green: 0.7411764706, blue: 0.5490196078, alpha: 1)
        label2.numberOfLines = 2
        label2.text = "Used and near new secondhand books at great prices."
        stackViewForLabels.addArrangedSubview(label2)
        
        stackViewForLabels.axis = .vertical
        stackViewForLabels.distribution = .fill
        stackViewForLabels.spacing = 20
        
        stackViewForAll.addArrangedSubview(stackViewForLabels)
        
        imgView.image = #imageLiteral(resourceName: "img1")
        imgView.contentMode = .scaleAspectFit
        stackViewForAll.addArrangedSubview(imgView)
        
       
        
        
    }
    
    
}
