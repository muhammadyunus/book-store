//
//  WalkthroughVC.swift
//  Book Store
//
//  Created by Rakhmatillo Topiboldiev on 8/11/20.
//  Copyright © 2020 Mukhammadyunus. All rights reserved.
//

import UIKit


class cWalkthroughVC: UIViewController {
    
    var collectionView: UICollectionView!
    var walkthroughObject : vWalkthroughView!
    var signInBtn : UIButton!
    var signUpBtn : UIButton!
    
    var data = [
        mWalkthroughDM(img: #imageLiteral(resourceName: "img1"), title: "Discounted Secondhand Books", subTitle: "Used and near new secondhand books at great prices."),
        mWalkthroughDM(img: #imageLiteral(resourceName: "img2"), title: "20 Book Grocers Nationally", subTitle: "We've successfully opened 20 stores across Australia."),
        mWalkthroughDM(img: #imageLiteral(resourceName: "img3"), title: "Sell or Recycle Your Old Books With Us", subTitle: "If you're looking to downsize, sell or recycle old books, the Book Grocer can help.")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        walkthroughObject = vWalkthroughView(view: self.view)
        collectionView = walkthroughObject.collectionView
        signInBtn = walkthroughObject.signInBtn
        signUpBtn = walkthroughObject.signUpBtn
        signUpBtn.addTarget(self, action: #selector(signUpTapped(_ :)), for: .touchUpInside)
        signInBtn.addTarget(self, action: #selector(signInTapped(_ :)), for: .touchUpInside)
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    
    
    //MARK: - SCROLLED HORIZONTALLY
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var page = round(scrollView.contentOffset.x / scrollView.frame.width)
        if !(page <= 3){
           page = 3
        }
        walkthroughObject.changePageController(page: page)
    }
    
    
   
     //MARK: -  OBJC FUNCTIONS
     @objc func signInTapped(_: UIButton!){
         print("HI")
         
     }
     @objc func signUpTapped(_: UIButton!){
         print("Bye")
         
     }
  
  
}


//MARK: - CollectionView Delegate
extension cWalkthroughVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CVC", for: indexPath) as? cWalkthroughCVC else {return UICollectionViewCell()}
        
        if indexPath.item == data.count{
            cell.contentView.isHidden = true
            return cell
        }else{
            cell.contentView.isHidden = false
            cell.updateCell(data: data[indexPath.item])
            return cell
        }
        
        
    }
    
 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    
}
